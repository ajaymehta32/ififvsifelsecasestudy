package ajaymehta.ififvsifelse;

/**
 * Created by Avi Hacker on 7/13/2017.
 */

public class Program2 { // like in saddle point of matrix (SaddlePoint) when we use loop and boolean variable together...

    public static void main(String args[]) {
        int arr[] = {2,5,6,7,8,3,9};
        printLarge(arr);

    }

    public static void printLarge(int arr[]) {


        boolean flag = true;

        int largeElement = 9; // we are checking if array has got any element bigger than it  ..we not taking any element from array

        for(int i=0; i< arr.length ; i++) {

            if(largeElement > arr[i]) {

                flag = false;
            } else {
                flag = true;  // if our element n array element is same..desipite of they are larger or bigger..flag become true ..n our answer will be wrong..
            }



        } // end of for loop

        if(!flag) {
            System.out.println("We have the large element  "+largeElement);
        } else {

            System.out.println("We didnt have the large element..");
        }
    }


}

// the answer of this program is didnt have the large element..but if you check in array 9 is our bigger elemnt (ya we know array has got 9 too...but not more than 9 ..so our element should be shown bigger..
// but its not saying that ..coz ...2 reasons...
// we took if else n arrays last element is 9 too...
/*
so what gonna happen is ..  our element (9) > arraylast element (9) {
                                    NO ...so its going in the else n making the flag true;  n flag true means  we didnt have large element

}*/
//