package ajaymehta.ififvsifelse;

/**
 * Created by Avi Hacker on 7/13/2017.
 */

public class Program2Alternative { // like in saddle point of matrix (SaddlePoint) when we use loop and boolean variable together...

    public static void main(String args[]) {
        int arr[] = {2,5,6,7,8,3,9};
        printLarge(arr);

    }

    public static void printLarge(int arr[]) {


        boolean flag = true;

        int largeElement = 9; // we are checking if array has got any element bigger than it  ..we not taking any element from array

        for(int i=0; i< arr.length ; i++) {

            if(largeElement > arr[i]) {  // otherwise take three condition..

                flag = false;
            } else if ( largeElement == arr[i]) {  // when we get our element equal to array element then do nothing...
                // do nothing..

            } else {
                flag = true;
            }



        } // end of for loop

        if(!flag) {
            System.out.println("We have the large element  "+largeElement);
        } else {

            System.out.println("We didnt have the large element..");
        }
    }


}

