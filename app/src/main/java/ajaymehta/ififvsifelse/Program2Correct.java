package ajaymehta.ififvsifelse;

/**
 * Created by Avi Hacker on 7/13/2017.
 */

public class Program2Correct { // like in saddle point of matrix (SaddlePoint) when we use loop and boolean variable together...

    public static void main(String args[]) {
        int arr[] = {2, 5, 6, 7, 8, 3, 9};
        printLarge(arr);

    }

    public static void printLarge(int arr[]) {


        boolean flag = true;

        int largeElement = 9; // so here when comparing our 9 > 9  ...okay it will not be true coz there also other conditon that will be checked ..if 9 < arrayelement 9 ....no ..so flag is not true .
// means if your element is equal to array element than .flag must not be true ..just a case study...

        for (int i = 0; i < arr.length; i++) {

            if (largeElement > arr[i]) {

                flag = false;
            }

            if(largeElement < arr[i]) {
                flag = true;
            }


        } // end of for loop

        if (!flag) {
            System.out.println("We have the large element  " + largeElement);
        } else {

            System.out.println("We didnt have the large element..");
        }
    }


}